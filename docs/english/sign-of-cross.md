
# Sign of the Cross

- [Latin](../latin/sign-of-cross-latin.md)

## Text

In the name of the Father, and of the Son, and of the Holy Spirit,

Amen.

