
# Saint Michael

- [Latin](../latin/saint-michael-latin.md)

## Text

- Saint Michael, the archangel, defend us in battle.
- Be our protection against the wickedness and snares of the Devil.
- May God rebuke him, we humbly pray, and
- do thou, O prince of the heavenly host, by the power of God,
- cast into hell Satan and all of the evil spirits
- who prowl through the world seeking the ruin of souls.

