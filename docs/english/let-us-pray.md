
# Let Us Pray (Rosary)

- [Latin](../latin/let-us-pray-latin.md)
- [Spanish](../spanish/let-us-pray-spanish.md)
- [German](../german/let-us-pray-german.md)

## Text

- Let us pray.

- O God,
  - whose only begotten Son,
  - by His life, death, and resurrection,
  - has purchased for us the rewards of eternal life,

- grant, we beseech Thee, that
  - meditating upon these mysteries
    - of the Most Holy Rosary of the Blessed Virgin Mary,
  - we may imitate what they contain and obtain what they promise,
  - through the same Christ Our Lord.

- Amen.

