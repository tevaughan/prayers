
# Hail Mary

- [Latin](../latin/hail-mary-latin.md)

## Text

- Hail Mary, full of grace.

- The Lord is with thee.

- Blessed art thou amongst women, And blessed is the fruit of thy womb,
  Jesus.

- Holy Mary, Mother of God, Pray for us sinners, Now and at the hour
  of our death.

- Amen.

