
# Apostle's Creed

- [Latin](../latin/apostles-creed-latin.md)
- [German](../german/apostles-creed-german.md)

## Text

- I believe
  - in God, the Father almighty, Creator of heaven and earth, and
  - in Jesus Christ, His only Son, our Lord, who
    - was conceived by the Holy Spirit,
    - was born of the Virgin Mary,
    - suffered under Pontius Pilate,
    - was crucified, died, and was buried.
    - He descended into hell.
    - On the third day he rose again from the dead.
    - He ascended into heaven and
    - is seated at the right hand of God, the Father almighty;
    - from there he will come to judge the living and the dead.

- I believe in
  - the Holy Spirit,
  - the Holy Catholic Church,
  - the communion of saints,
  - the forgiveness of sins,
  - the resurrection of the body, and
  - the life everlasting.

- Amen.

## Notes

The text is taken from [the Rosary Center &
Confraternity](https://rosarycenter.org/prayers-of-the-rosary).

The use of "hell" is interesting in light of the Latin text in the Roman
Breviary of 1568 after the Council of Trent.  The Breviary uses "inferos"
(the ones below) rather than "inferna" (hell).

Changes:

- Text is arranged into bulleted lines, with one sentence for each major
  bullet and with each parallel list of things arranged as an array of
  sub-bullets.

- A comma was added after "earth" because that completes an appositive
  phrase (the second of two appositive phrases, each referring to God).

- The word "was" was inserted before "born of the Virgin Mary" so that every
  element in the list of verb-phrases would serve as the proper predicate for
  "who".

