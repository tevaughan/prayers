
# Goals

## Initial

- To render all of the prayers for the rosary in each of
  - English,
  - Latin,
  - Spanish, and
  - German.

- For each non-English rendering, to render an English interlinear.
  - I have done this already for the Rosary's Let Us Pray in each of
    - the [Latin](latin/let-us-pray-latin.md),
    - the [Spanish](spanish/let-us-pray-spanish.md), and
    - the [German](german/let-us-pray-german.md).

- To make notes on the history of each prayer and on details of its rendering
  in each language.
  - I have done this already for [the Latin version of the Apostle's
    Creed](latin/apostles-creed-latin.md).

## Further

- To add Greek, at least for those prayers that might have been written in
  Greek in antiquity.

- To add other languages, especially if folk volunteer to help.

- To add chant-settings at least for Latin with Gregorian notation, and
  possibly for other languages, to assist one in singing the rosary and other
  common prayers.

