
# Oremus (Rosario)

- V. Orémus:
- (V. Let us pray:)

- Déus, cújus Unigénitus,
- (God, whose Only Begotten Son,)

- per vítam, mórtem, et resurrectiónem súam,
- (through life, death, and resurrection his,)

- nóbis salútis aetérnae praemia comparávit,
- (for us of salvation eternal the reward has obtained,)

- concéde, quaesumus:
- (grant, we beg:)

- ut haec mystéria sacratíssimo
- (so by these mysteries of the most holy)

- beátae Maríae Vírginis Rosário recoléntes,
- (blessed Mary Virgin's Rosary celebrating,)

- et imitémur quod cóntinent,
- (that we may imitate what they contain,)

- et quod promíttunt assequámur.
- (and what they promise may obtain.)

- Per eúndem Chrístum Dóminum nóstrum.
- (Through the same Christ Lord our.)

Amen.

