
# Oratio Fatimae

- O (mi) Bóne Iésu,

- líbera nos a peccatís nóstris;
- líbera nos ab ígnibus gehénnae;
- perdúc in paradísum ómnes animás;
- praesértim eas quae plus misericórdia tua indígent!

## Notes

This version is different from the one that I am familiar with.  I need to do
some research on this.

