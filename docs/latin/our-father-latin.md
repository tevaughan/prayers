
# Pater Noster

- Páter Nóster,
- Qui es in cáelis,
- sanctificétur nómen túum.
- Advéniat régnum túum,
- fíat volúntus túa,
- sícut in cáelo et in térra.

- Pánem nóstrum quotidiánum da nóbis hódie,
- et dimítte nóbis débita nóstra,
- sícut et nos dimíttimus debitóribus nóstris.
- Et ne nos indúcas in tentatiónem:
- sed líbera nos a málo.

Amen.

