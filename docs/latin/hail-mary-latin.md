
# Ave Maria

- Áve María,
- grátia pléna,
- Dóminus técum.
- Benedícta tu in muliéribus,
- et benedíctus frúctus véntris túi, Iésus.

- Sáncta María,
- Máter Déi,
- óra pro nóbis peccatóribus,
- nunc, et in hóra mórtis nóstræ.

Amen.

