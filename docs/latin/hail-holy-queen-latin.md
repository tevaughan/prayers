
# Salve Regina

- Sálve Regína, Máter misericórdiae!
- Víta, dulcédo, et spes nóstra, sálve.
- Ad te clamámus, éxsules filii Hévae.
- Ad te suspirámus,
- geméntes et fléntes in hac lacrimárum válle.

- Eia érgo, Advocáta nóstra,
- íllos túos misericórdes óculos ad nos convérte.
- Et Iésum, benedíctum frúctum véntris túi,
- nóbis post hoc exsílium osténde.

- O clémens, O pía, O dúlcis Vírgo María!

- V/. Óra pro nóbis, Sáncta Déi Génetrix.
- R/. Ut dígni efficiámur promissiónibus Chrísti. Amen.

