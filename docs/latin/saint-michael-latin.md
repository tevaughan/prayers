
# Sancte Michael

Sáncte Micháel, Archángele,

- defénde nos in proélio.
- Contra nequítiam et insídias diáboli esto praesídium.
- Impéret illi Déus, súpplices deprecámur,
- et tuque, Prínceps milítiae caeléstis,
- Sátanam aliósque spíritus malígnos,
- qui ad perditiónem animárum pervagántur in mundo,
- divína virtúte, in inférnum detrúde.

Amen.

