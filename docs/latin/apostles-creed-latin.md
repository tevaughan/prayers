
# Credo

- [English](../english/apostles-creed.md)
- [German](../german/apostles-creed-german.md)

## Text

- Credo
  - in Deum, Patrem omnipoténtem, Creatórem caeli et terrae, et
  - in Iesum Christum, Fílium eius únicum, Dóminum nostrum, qui
    - concéptus est de Spíritu Sancto,
    - natus ex María Vírgine,
    - passus sub Póntio Piláto,
    - crucifíxus, mórtuus, et sepúltus;
    - descéndit ad ínferos;
    - tértia die resurréxit a mórtuis;
    - ascéndit ad caelos;
    - sedet ad déxteram Dei Patris omnipoténtis,
    - inde venturus est iudicare vivos et mortuos.

- Credo in
  - Spíritum Sanctum,
  - sanctam Ecclésiam Cathólicam,
  - sanctórum communiónem,
  - remissiónem peccatórum,
  - carnis resurrectiónem, et
  - vitam aetérnam.

- Amen.

## Notes

According to Denzinger's *Enchiridion* (43rd Edition, Latin-English, pp
26-27), this form was adopted by Rome in the Tenth Century from the Gallican
liturgy after the ancient Roman liturgical tradition had been interrupted.

The precise form was specified for the whole Latin Church in the Roman
Breviary (1568), "in order to remove deviations in prayer," just after the
Council of Trent.  The notable change in the Breviary was from "inferna" to
"inferos;" that is, from "hell" to "those who are below" or "the dead."  A
common translation into English still in use today seems to ignore the
Breviary and to use "hell" rather than "the dead."

My version is exactly that of the Breviary except for a couple of minor
changes:

- The "et" before "et vitam aeternam."  This "et" does not appear in the
  Breviary but does appear in the Ordo Romano Antico from the Tenth Century.

- Punctuation.  Commas were changed to semicolons to separate what I think of
  as clauses, rather than mere verb-phrases.

- Accent-marks were taken from the [the Rosary Center &
  Confraternity](https://rosarycenter.org/prayers-of-the-rosary).  The Rosary
  Center uses "infernos" instead of "inferos", but the meaning is the same,
  "those below", as in the Breviary.

