
# Gloria Patri

Glória Pátri, et Fílio, et Spirítui Sáncto.

- Sícut érat in princípio,
- et nunc, et sémper,
- et in sáecula sæculórum.

Amen.

