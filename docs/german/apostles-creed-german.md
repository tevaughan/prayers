
# Apostolische Glaubensbekenntnis

- [English](../english/apostles-creed.md)
- [Latin](../latin/apostles-creed-latin.md)

# Text

- Ich glaube
  - an Gott, den Vater, den Allmächtigen, den Schöpfer des Himmels und der
    Erde, und
  - an Jesus Cristus, seinen eingeborenen Sohn, unsern Herrn,
    - empfangen durch den Heiligen Geist,
    - geboren von der Jungfrau Maria,
    - gelitten unter Pontius Pilatus,
    - gekreuzigt, gestorben, und begraben,
    - hinabgestiegen in das Reich des Todes,
    - am dritten Tage auferstanden von den Toten,
    - aufgefahren in den Himmel;
    - er sitzt zur Rechten Gottes, des allmächtigen Vaters;
    - von dort wird er kommen, zu richten die Lebenden und die Toten.

- Ich glaube an
  - den Heiligen Geist,
  - die heilige katolische Kirche,
  - Gemeinschaft der Heiligen,
  - Vergebung der Sünde,
  - Auferstehung der Toten, und
  - das ewige Leben.

- Amen.

## Notes

The text is taken from [the Rosary Center &
Confraternity](https://rosarycenter.org/prayers-of-the-rosary).

Neither the German equivalent of "Hell" nor simply "the dead" nor "those
below" is used.  Rather, "das Reich des Todes", "the kingdom of the dead".

My version uses the Oxford comma, and so I inserted two commas that were not
present in the version at the Rosary Center.

