
# Lasset Uns Beten (Rosenkranz)

- V. Lasset uns beten.
- (V. Let us pray.)

- O Gott, dessen eingeborener Sohn,
- (O God, whose only-begotten Son,)

- durch sein Leben, seinen Tod und seine Auferstehung,
- (through his Life, his Death, and his Resurrection,)

- uns den Lohn des ewigen Heils erworben hat,
- (for us the Reward of the eternal Salvation acquired has,)

- gewähre uns, bitten wir Dich,
- (grant us, beseech we Thee,)

- dass wir durch das Betrachten der Geheimnisse
- (that we, through the Contemplation of the Mysteries)

- des allerheiligsten Rosenkranzes
- (of the most-holy-of-all Rosary)

- der seligen Jungfrau Maria
- (of the blessed Virgin Mary,)

- nachahmen was sie enthalten,
- (may imitate what they contain,)

- und erlangen was sie verheißen.
- (and obtain what they promise.)

- Durch denselben Christus, unseren Herrn.
- (Through the same Christ, our Lord.)

- Amen.

