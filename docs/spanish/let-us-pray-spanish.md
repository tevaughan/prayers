
# Oremos (Rosario)

- V. Oremos.
- (V. Let us pray.)

- Oh Dios, cuyo Hijo unigénito,
- (Oh God, whose Son only begotten,)

- con su vida, muerte y resurrección,
- (through His life, death, and resurrection,)

- nos ha merecido el premio de la salvación eterna,
- (for us has obtained the rewards of the salvation eternal,)

- concédenos, te suplicamos,
- (grant, Thee we beseech,)

- que meditando los misterios del Santísimo Rosario
- (that contemplating the mysteries of the Most Holy Rosary)

- de la bienaventurada Virgen María,
- (of the Blessed Virgin Mary,)

- imitemos lo que contienen
- (we may imitate what they contain)

- y alcancemos lo que prometen.
- (and obtain what they promise.)

- Por el mismo Cristo nuestro Señor.
- (Through the same Christ our Lord.)

- Amén.

