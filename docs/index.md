
# Prayer and Language

For each of various prayers, one may find here a rendering of it in each of a
few languages.

Each rendering has non-standard punctuation (such as bullets) and formatting
(such as line-breaks) intended to maximize both the comprehension and the
ease of comparison of different language-renderings for the same prayer.

This site was set up on 2023 Oct 31 and might not have the initial goals
accomplished until until 2023 Christmas.

- [Motivation](motivation.md)
- [Goals](goals.md)

## Prayers

- [Sign of the Cross](english/sign-of-cross.md)
- [Apostle's Creed](english/apostles-creed.md)
- [Our Father](english/our-father.md)
- [Hail Mary](english/hail-mary.md)
- [Glory Be](english/glory-be.md)
- [Fatima](english/fatima.md)
- [Hail, Holy Queen](english/hail-holy-queen.md)
- [Let Us Pray (Rosary)](english/let-us-pray.md)
- [Saint Michael](english/saint-michael.md)

## External Resources

- [Source-code][source] for this site
- [Rosary Center & Confraternity][confrat]
- [Latin prayers at EWTN (no accent-marks)][Latin-EWTN]
- [How to pray Rosary in Latin (TraditionalCatholicPrayers.com,
  accent-marks)][Rosary-Latin]
- [Booklet][st-marys-rosary] on Latin rosary at St. Mary's Parish, Tacoma,
  Washington

[source]: https://gitlab.com/tevaughan/prayers
[confrat]: https://rosarycenter.org/prayers-of-the-rosary
[Latin-EWTN]: https://www.ewtn.com/catholicism/library/latin-prayers-9125
[Rosary-Latin]: https://traditionalcatholicprayers.com/2019/11/12/how-to-pray-the-rosary-in-latin/#comment-21937
[st-marys-rosary]: http://stmarys-parish.org/Latin/The%20Rosary.pdf

