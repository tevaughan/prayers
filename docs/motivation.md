
# Motivation

- The project started as a resource for my rosary-group.

- As the overall leader of the rosary, I typically change, from day to day,
  the language that I use for the introductory and concluding prayers.

- I encourage each leader of a decade to change languages as he moves through
  the prayers of his decade.

