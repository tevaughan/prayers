# Prayer and Language

Repository of source-code for my [site that renders each of various prayers
in each of a few languages][prayers].

[prayers]: https://tevaughan.gitlab.io/prayers

- This project started as a resource for my rosary-group.
- We like to change languages as we move through the prayers of the rosary.

## Annotated List of Top-Level Contents

- [docs](docs) is a directory containing markdown-source for the deployed
  site.
  - Because the site uses MkDocs, the markdown for the site is
    python-markdown.
  - Python-markdown is by default quite primitive, especially as regards
    lists.
  - Each of `requirements.txt`, `.gitlab-ci.yml`, and `mkdocs.yml` mentions
    `mdx_truly_sane_lists`, a python-package providing a markdown-extension
    that allows for nesting of lists and generates for lists better HTML.
  - The markdown-extension packages, `meta`, `toc`, `tables`, and
    `fenced_code` are all available by default in MkDocs

- [.gitignore](.gitignore) contains names of local resources for git to
  ignore.

- [.gitlab-ci.yml](.gitlab-ci.yml) contains instructions for gitlab's
  automated deployment of the site to gitlab-pages.

- [mkdocs.yml](mkdocs.yml) contains configuration for MkDocs, the static-site
  generator.

- [requirements.txt](requirements.txt) contains list of python-packages (and
  a minimum version for each) for `pip` to install into virtual environment
  in local copy for local development.
  ```sh
  virtualenv python-venv
  source python-venv/bin/activate
  pip install -r requirements.txt
  ```

## Local Development

- After virtual environment has been configured as above, every time one
  would do local development, one would open a shell, change directory into
  the local copy:
  ```sh
  source python-venv/bin/activate
  mkdocs serve
  ```

- Then point the browser at `localhost:8000`.
- The site will update live as markdown-files are edited.

